# Exo

## Url

http://localhost:8080

Trouver les urls des Apis. 
Coder tous les Controller et Repository nécessaires pour faire fonctionner le front

## Model

### PAtient
{
	"id": 33,
	"name": "m",
	"forename": "a"
}

### Visit
{
	"id": 36,
	"date": "2019-01-10",
	"type": "BILAN",
	"patient": {
		"id": 33,
		"name": "m",
		"forename": "a"
	},
	"price": 10.0,
	"receiveToday": 10.0,
	"paymentMethod": "CHEQUE",
	"comment": "string"
}

