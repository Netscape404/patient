(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-layout></app-layout>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ui_ui_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ui/ui.module */ "./src/app/ui/ui.module.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _contact_contact_component__WEBPACK_IMPORTED_MODULE_6__["ContactComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"], _ui_ui_module__WEBPACK_IMPORTED_MODULE_5__["UiModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/contact/contact.component.css":
/*!***********************************************!*\
  !*** ./src/app/contact/contact.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/contact/contact.component.html":
/*!************************************************!*\
  !*** ./src/app/contact/contact.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\n  contact\n</div>\n"

/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ContactComponent = /** @class */ (function () {
    function ContactComponent() {
    }
    ContactComponent.prototype.ngOnInit = function () {
    };
    ContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__(/*! ./contact.component.html */ "./src/app/contact/contact.component.html"),
            styles: [__webpack_require__(/*! ./contact.component.css */ "./src/app/contact/contact.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "./src/app/ui/footer/footer.component.css":
/*!************************************************!*\
  !*** ./src/app/ui/footer/footer.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fa-heart {\n  color: hotpink;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdWkvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtDQUNoQiIsImZpbGUiOiJzcmMvYXBwL3VpL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mYS1oZWFydCB7XG4gIGNvbG9yOiBob3RwaW5rO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/ui/footer/footer.component.html":
/*!*************************************************!*\
  !*** ./src/app/ui/footer/footer.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark bg-dark mt-5 fixed-bottom\">\n  <div class=\"navbar-expand m-auto navbar-text\">\n    Made with <i class=\"fa fa-heart\"></i> by Arnaud.\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/ui/footer/footer.component.ts":
/*!***********************************************!*\
  !*** ./src/app/ui/footer/footer.component.ts ***!
  \***********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/ui/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/ui/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/ui/header/header.component.css":
/*!************************************************!*\
  !*** ./src/app/ui/header/header.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/ui/header/header.component.html":
/*!*************************************************!*\
  !*** ./src/app/ui/header/header.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark bg-dark mb-5\">\n  <span class=\"navbar-brand\" href=\"#\">Mes patients</span>\n  <div class=\"navbar-expand mr-auto\">\n    <div class=\"navbar-nav\">\n      <a class=\"nav-item nav-link\"  routerLink=\"/patient\" routerLinkActive=\"active\">Patient</a>\n      <a class=\"nav-item nav-link\"  routerLink=\"/visits\" routerLinkActive=\"active\">Consultation</a>\n      <a class=\"nav-item nav-link\"  routerLink=\"/contact\" routerLinkActive=\"active\">Contact</a>\n    </div>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/ui/header/header.component.ts":
/*!***********************************************!*\
  !*** ./src/app/ui/header/header.component.ts ***!
  \***********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/ui/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/ui/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/ui/layout/layout.component.css":
/*!************************************************!*\
  !*** ./src/app/ui/layout/layout.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL2xheW91dC9sYXlvdXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/ui/layout/layout.component.html":
/*!*************************************************!*\
  !*** ./src/app/ui/layout/layout.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"container\">\n  <ng-content></ng-content>\n  <router-outlet></router-outlet>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/ui/layout/layout.component.ts":
/*!***********************************************!*\
  !*** ./src/app/ui/layout/layout.component.ts ***!
  \***********************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LayoutComponent = /** @class */ (function () {
    function LayoutComponent() {
    }
    LayoutComponent.prototype.ngOnInit = function () {
    };
    LayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./layout.component.html */ "./src/app/ui/layout/layout.component.html"),
            styles: [__webpack_require__(/*! ./layout.component.css */ "./src/app/ui/layout/layout.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "./src/app/ui/patient/patient.component.css":
/*!**************************************************!*\
  !*** ./src/app/ui/patient/patient.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL3BhdGllbnQvcGF0aWVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/ui/patient/patient.component.html":
/*!***************************************************!*\
  !*** ./src/app/ui/patient/patient.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col\">\n    <h1>Nouveau patient</h1>\n    <form [formGroup]=\"patientForm\" (ngSubmit)=\"addPatient()\">\n      <div class=\"form-group\">\n        <label for=\"name\">Nom</label>\n        <input type=\"text\" class=\"form-control\" [class.is-invalid]=\"name.invalid && name.touched\" id=\"name\"\n               placeholder=\"Entrer le nom\" formControlName=\"name\">\n        <div *ngIf=\"name.invalid && name.touched\" class=\"invalid-feedback\">Veuillez entrer le nom.</div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"forename\">Prénom</label>\n        <input type=\"text\" class=\"form-control\" id=\"forename\" placeholder=\"Entrer le prénom\" formControlName=\"forename\">\n      </div>\n\n      <input type=\"hidden\" id=\"id\" formControlName=\"id\">\n      <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"patientForm.invalid\">Enregistrer\n      </button>\n    </form>\n  </div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col\">\n    <h1>Patients</h1>\n    <table class=\"table\">\n      <thead class=\"thead-light\">\n      <tr>\n        <th scope=\"col\">#</th>\n        <th scope=\"col\">Prénom</th>\n        <th scope=\"col\">Nom</th>\n        <th scope=\"col\"></th>\n        <th scope=\"col\"></th>\n        <th scope=\"col\"></th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let patient of patients\">\n        <td>{{patient.id}}</td>\n        <td>{{patient.forename}}</td>\n        <td>{{patient.name}}</td>\n        <td>\n          <div style=\"cursor: pointer;\" (click)=\"editPatient(patient)\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></div>\n        </td>\n        <td>\n          <div style=\"cursor: pointer;\" (click)=\"removePatient(patient)\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></div>\n        </td>\n        <td>\n          <a [routerLink]=\"['/patient', patient.id, 'visit']\"><button type=\"button\" class=\"btn btn-primary btn-sm\" >Consultation</button></a>\n        </td>\n      </tr>\n\n      </tbody>\n    </table>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/ui/patient/patient.component.ts":
/*!*************************************************!*\
  !*** ./src/app/ui/patient/patient.component.ts ***!
  \*************************************************/
/*! exports provided: PatientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientComponent", function() { return PatientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patient.service */ "./src/app/ui/patient/patient.service.ts");




var PatientComponent = /** @class */ (function () {
    function PatientComponent(fb, patientService) {
        this.fb = fb;
        this.patientService = patientService;
        this.patients = [];
    }
    PatientComponent.prototype.ngOnInit = function () {
        this.patientForm = this.fb.group({
            id: '',
            name: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2)
                ]],
            forename: ''
        });
        this.getPatients();
    };
    PatientComponent.prototype.getPatients = function () {
        var _this = this;
        this.patients = [];
        this.patientService.getPatients()
            .subscribe(function (data) { return _this.patients = data; });
        return this.patients;
    };
    PatientComponent.prototype.addPatient = function () {
        var _this = this;
        var newPatient = Object.assign({}, this.patientForm.value);
        console.log(newPatient);
        if (newPatient.id) {
            this.patientService.updatePatient(newPatient)
                .subscribe(function (patient) {
                console.log('Visit updated ' + patient);
                _this.getPatients();
                _this.patientForm.reset();
            });
        }
        else {
            this.patientService.addPatient(newPatient)
                .subscribe(function (patient) {
                console.log('Visit added ' + patient);
                _this.getPatients();
                _this.patientForm.reset();
            });
        }
    };
    Object.defineProperty(PatientComponent.prototype, "name", {
        get: function () {
            return this.patientForm.get('name');
        },
        enumerable: true,
        configurable: true
    });
    PatientComponent.prototype.editPatient = function (patient) {
        console.log('Visit to update ' + patient.id);
        this.patientForm.setValue(patient);
    };
    PatientComponent.prototype.removePatient = function (patient) {
        var _this = this;
        console.log('Visit to remove ' + patient.id);
        this.patientService.removePatient(patient)
            .subscribe(function (removedPatient) {
            console.log('Visit removed ' + removedPatient);
            _this.getPatients();
        });
    };
    PatientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-patient',
            template: __webpack_require__(/*! ./patient.component.html */ "./src/app/ui/patient/patient.component.html"),
            styles: [__webpack_require__(/*! ./patient.component.css */ "./src/app/ui/patient/patient.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"]])
    ], PatientComponent);
    return PatientComponent;
}());



/***/ }),

/***/ "./src/app/ui/patient/patient.service.ts":
/*!***********************************************!*\
  !*** ./src/app/ui/patient/patient.service.ts ***!
  \***********************************************/
/*! exports provided: PatientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientService", function() { return PatientService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");






var PatientService = /** @class */ (function () {
    function PatientService(http) {
        this.http = http;
        this.configUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].serviceUrl + '/patients';
    }
    PatientService.prototype.getPatients = function () {
        return this.http.get(this.configUrl);
    };
    PatientService.prototype.getPatient = function (id) {
        var urlById = this.configUrl + '/' + id;
        return this.http.get(urlById);
    };
    PatientService.prototype.addPatient = function (patient) {
        return this.http.post(this.configUrl, patient)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    PatientService.prototype.updatePatient = function (patient) {
        return this.http.put(this.configUrl, patient)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    PatientService.prototype.removePatient = function (patient) {
        var url = this.configUrl + "/" + patient.id;
        return this.http.delete(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    PatientService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    };
    ;
    PatientService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], PatientService);
    return PatientService;
}());



/***/ }),

/***/ "./src/app/ui/patient/patient.ts":
/*!***************************************!*\
  !*** ./src/app/ui/patient/patient.ts ***!
  \***************************************/
/*! exports provided: Patient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Patient", function() { return Patient; });
var Patient = /** @class */ (function () {
    function Patient() {
    }
    return Patient;
}());



/***/ }),

/***/ "./src/app/ui/ui.module.ts":
/*!*********************************!*\
  !*** ./src/app/ui/ui.module.ts ***!
  \*********************************/
/*! exports provided: UiModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiModule", function() { return UiModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _layout_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout/layout.component */ "./src/app/ui/layout/layout.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/header.component */ "./src/app/ui/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/ui/footer/footer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _patient_patient_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./patient/patient.component */ "./src/app/ui/patient/patient.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./patient/patient.service */ "./src/app/ui/patient/patient.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _visit_visit_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./visit/visit.component */ "./src/app/ui/visit/visit.component.ts");
/* harmony import */ var _visit_new_new_visit_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./visit/new/new-visit.component */ "./src/app/ui/visit/new/new-visit.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/locales/fr */ "./node_modules/@angular/common/locales/fr.js");
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _angular_common_locales_extra_fr__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/locales/extra/fr */ "./node_modules/@angular/common/locales/extra/fr.js");
/* harmony import */ var _angular_common_locales_extra_fr__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_extra_fr__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _visit_list_visit_list_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./visit-list/visit-list.component */ "./src/app/ui/visit-list/visit-list.component.ts");


















var appRoutes = [
    { path: 'patient', component: _patient_patient_component__WEBPACK_IMPORTED_MODULE_8__["PatientComponent"] },
    { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__["ContactComponent"] },
    { path: 'patient/:patientId/visit', component: _visit_visit_component__WEBPACK_IMPORTED_MODULE_12__["VisitComponent"] },
    { path: 'patient/:patientId/new-visit', component: _visit_new_new_visit_component__WEBPACK_IMPORTED_MODULE_13__["NewVisitComponent"] },
    { path: 'patient/:patientId/new-visit/:visitId', component: _visit_new_new_visit_component__WEBPACK_IMPORTED_MODULE_13__["NewVisitComponent"] },
    { path: 'visits', component: _visit_list_visit_list_component__WEBPACK_IMPORTED_MODULE_17__["VisitListComponent"] },
];
Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["registerLocaleData"])(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_15___default.a, 'fr');
Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["registerLocaleData"])(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_15___default.a, 'fr-FR', _angular_common_locales_extra_fr__WEBPACK_IMPORTED_MODULE_16___default.a);
var UiModule = /** @class */ (function () {
    function UiModule() {
    }
    UiModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            exports: [_layout_layout_component__WEBPACK_IMPORTED_MODULE_3__["LayoutComponent"]],
            providers: [_patient_patient_service__WEBPACK_IMPORTED_MODULE_10__["PatientService"], { provide: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbDateAdapter"], useClass: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbDateNativeAdapter"] }, { provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["LOCALE_ID"], useValue: 'fr' }],
            declarations: [_layout_layout_component__WEBPACK_IMPORTED_MODULE_3__["LayoutComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"], _patient_patient_component__WEBPACK_IMPORTED_MODULE_8__["PatientComponent"], _visit_visit_component__WEBPACK_IMPORTED_MODULE_12__["VisitComponent"], _visit_new_new_visit_component__WEBPACK_IMPORTED_MODULE_13__["NewVisitComponent"], _visit_list_visit_list_component__WEBPACK_IMPORTED_MODULE_17__["VisitListComponent"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(appRoutes),
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbModule"]
            ]
        })
    ], UiModule);
    return UiModule;
}());



/***/ }),

/***/ "./src/app/ui/visit-list/visit-list.component.css":
/*!********************************************************!*\
  !*** ./src/app/ui/visit-list/visit-list.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL3Zpc2l0LWxpc3QvdmlzaXQtbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/ui/visit-list/visit-list.component.html":
/*!*********************************************************!*\
  !*** ./src/app/ui/visit-list/visit-list.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col\">\n    <h1>Consultations du {{date | date}}</h1>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"col\">\n    <form [formGroup]=\"visitForm\" (ngSubmit)=\"searchVisit()\">\n\n      <div class=\"form-group\">\n        <label for=\"date\">Date de la séance</label>\n        <div class=\"input-group\">\n          <input class=\"form-control\" formControlName=\"date\" id=\"date\"\n                 name=\"dp\" ngbDatepicker #dp=\"ngbDatepicker\">\n          <div class=\"input-group-append\">\n            <button class=\"btn btn-outline-secondary calendar\" (click)=\"dp.toggle()\" type=\"button\"><i\n              class=\"fa fa-calendar\" aria-hidden=\"true\"></i></button>\n          </div>\n        </div>\n      </div>\n      <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"visitForm.invalid\">\n        Rechercher\n      </button>\n    </form>\n  </div>\n</div>\n\n<hr class=\"nb-1\">\n<div class=\"row\">\n  <div class=\"col\">\n    <table class=\"table\">\n      <thead class=\"thead-light\">\n      <tr>\n        <th scope=\"col\">#</th>\n        <th scope=\"col\">Date</th>\n        <th scope=\"col\">Type</th>\n        <th scope=\"col\">Montant de la prestation</th>\n        <th scope=\"col\">Reçu aujourd'hui</th>\n        <th scope=\"col\">Méthode de paiement</th>\n        <th scope=\"col\">Commentaire</th>\n        <th scope=\"col\"></th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let visit of visits\">\n        <td>{{visit.id}}</td>\n        <td>{{visit.date | date}}</td>\n        <td>{{visit.type}}</td>\n        <td>{{visit.price}}</td>\n        <td>{{visit.receiveToday}}</td>\n        <td>{{visit.paymentMethod}}</td>\n        <td>{{visit.comment}}</td>\n        <td>\n          <a [routerLink]=\"['/patient', visit.patient.id, 'new-visit', visit.id]\">\n            <div style=\"cursor: pointer;\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></div>\n          </a>\n        </td>\n      </tr>\n\n      </tbody>\n    </table>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/ui/visit-list/visit-list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/ui/visit-list/visit-list.component.ts ***!
  \*******************************************************/
/*! exports provided: VisitListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitListComponent", function() { return VisitListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _visit_visit_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../visit/visit.service */ "./src/app/ui/visit/visit.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _visit_new_ngbDateCustomParserFormatter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../visit/new/ngbDateCustomParserFormatter */ "./src/app/ui/visit/new/ngbDateCustomParserFormatter.ts");






var VisitListComponent = /** @class */ (function () {
    function VisitListComponent(visitService, fb) {
        this.visitService = visitService;
        this.fb = fb;
        this.date = new Date();
        this.visits = [];
    }
    VisitListComponent.prototype.ngOnInit = function () {
        this.visitForm = this.fb.group({
            date: new Date()
        });
        this.getVisits(this.date);
    };
    VisitListComponent.prototype.getVisits = function (date) {
        var _this = this;
        this.visits = [];
        this.visitService.getVisitByDate(date)
            .subscribe(function (data) { return _this.visits = data; });
        return this.visits;
    };
    VisitListComponent.prototype.searchVisit = function () {
        this.date = this.visitForm.value['date'];
        console.log('Search visit on date of : ' + this.date);
        this.getVisits(this.date);
    };
    VisitListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-visit-list',
            template: __webpack_require__(/*! ./visit-list.component.html */ "./src/app/ui/visit-list/visit-list.component.html"),
            providers: [
                { provide: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbDateParserFormatter"], useClass: _visit_new_ngbDateCustomParserFormatter__WEBPACK_IMPORTED_MODULE_5__["NgbDateCustomParserFormatter"] }
            ],
            styles: [__webpack_require__(/*! ./visit-list.component.css */ "./src/app/ui/visit-list/visit-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_visit_visit_service__WEBPACK_IMPORTED_MODULE_2__["VisitService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], VisitListComponent);
    return VisitListComponent;
}());



/***/ }),

/***/ "./src/app/ui/visit/new/new-visit.component.css":
/*!******************************************************!*\
  !*** ./src/app/ui/visit/new/new-visit.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL3Zpc2l0L25ldy9uZXctdmlzaXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/ui/visit/new/new-visit.component.html":
/*!*******************************************************!*\
  !*** ./src/app/ui/visit/new/new-visit.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col\">\n    <h1>{{editText}} consultation pour {{patient.forename}} {{patient.name}}</h1>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"col\">\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"col\">\n    <form [formGroup]=\"visitForm\" (ngSubmit)=\"addVisit()\">\n\n      <div class=\"form-group\">\n        <label for=\"date\">Date de la séance</label>\n        <div class=\"input-group\">\n          <input class=\"form-control\" formControlName=\"date\" id=\"date\"\n                 name=\"dp\" ngbDatepicker #dp=\"ngbDatepicker\">\n          <div class=\"input-group-append\">\n            <button class=\"btn btn-outline-secondary calendar\" (click)=\"dp.toggle()\" type=\"button\"><i\n              class=\"fa fa-calendar\" aria-hidden=\"true\"></i></button>\n          </div>\n        </div>\n      </div>\n\n      <fieldset class=\"form-group\">\n        <div class=\"row\">\n          <legend class=\"col-form-label col-sm-2 pt-0\">Consultation</legend>\n          <div class=\"col-sm-10\">\n            <div class=\"form-check form-check-inline\">\n              <input class=\"form-check-input\" type=\"radio\" formControlName=\"type\" value=\"SEANCE\" id=\"seance\"\n                     (click)=\"assignPrice($event)\">\n              <label class=\"form-check-label\" for=\"seance\">Séance</label>\n            </div>\n            <div class=\"form-check form-check-inline\">\n              <input class=\"form-check-input\" type=\"radio\" formControlName=\"type\" value=\"BILAN\" id=\"bilan\" (click)=\"assignPrice($event)\">\n              <label class=\"form-check-label\" for=\"bilan\">Bilan</label>\n            </div>\n            <div class=\"form-check form-check-inline\">\n              <input class=\"form-check-input\" type=\"radio\" formControlName=\"type\" value=\"ENTRETIEN\" id=\"entretien\" (click)=\"assignPrice($event)\">\n              <label class=\"form-check-label\" for=\"entretien\">Entretien</label>\n            </div>\n          </div>\n        </div>\n      </fieldset>\n\n      <div class=\"form-group\">\n        <label for=\"price\">Cout de la prestation</label>\n        <input type=\"text\" class=\"form-control\" id=\"price\"\n               placeholder=\"Cout de la prestation\" formControlName=\"price\">\n      </div>\n\n      <div class=\"form-group\">\n        <label for=\"receiveToday\">Perçu ce jour</label>\n        <input type=\"text\" class=\"form-control\" id=\"receiveToday\"\n               placeholder=\"Perçu ce jour\" formControlName=\"receiveToday\">\n      </div>\n\n      <div class=\"form-group\">\n        <label for=\"paymentMethod\">Méthode de paiement</label>\n        <select formControlName=\"paymentMethod\" id=\"paymentMethod\" class=\"form-control\">\n          <option value=\"DEFAULT\">Choisir un moyen de paiement</option>\n          <option *ngFor=\"let pm of paymentMethods()\">\n            {{ pm }}\n          </option>\n        </select>\n      </div>\n\n      <div class=\"form-group\">\n        <label for=\"comment\">Commentaire</label>\n        <textarea class=\"form-control\" id=\"comment\"\n                  placeholder=\"Commentaire\" formControlName=\"comment\"></textarea>\n      </div>\n\n      <input type=\"hidden\" id=\"id\" formControlName=\"id\">\n      <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"visitForm.invalid\">Enregistrer\n      </button>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/ui/visit/new/new-visit.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/ui/visit/new/new-visit.component.ts ***!
  \*****************************************************/
/*! exports provided: NewVisitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewVisitComponent", function() { return NewVisitComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _visit_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../visit.service */ "./src/app/ui/visit/visit.service.ts");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../patient/patient.service */ "./src/app/ui/patient/patient.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _paymentMethod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../paymentMethod */ "./src/app/ui/visit/paymentMethod.ts");
/* harmony import */ var _patient_patient__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../patient/patient */ "./src/app/ui/patient/patient.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _ngbDateCustomParserFormatter__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ngbDateCustomParserFormatter */ "./src/app/ui/visit/new/ngbDateCustomParserFormatter.ts");










var NewVisitComponent = /** @class */ (function () {
    function NewVisitComponent(router, fb, route, visitService, patientService) {
        this.router = router;
        this.fb = fb;
        this.route = route;
        this.visitService = visitService;
        this.patientService = patientService;
        this.patient = new _patient_patient__WEBPACK_IMPORTED_MODULE_7__["Patient"]();
    }
    NewVisitComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.visitForm = this.fb.group({
            id: '',
            date: new Date(),
            type: 'SEANCE',
            price: 30,
            receiveToday: 0,
            paymentMethod: null,
            comment: ''
        });
        var visitId = Number(this.route.snapshot.paramMap.get('visitId'));
        var patientId = Number(this.route.snapshot.paramMap.get('patientId'));
        if (visitId) {
            console.log('Edit visit for patient: ' + patientId + ' and visit: ' + visitId);
            this.editText = 'Modification de la ';
        }
        else {
            console.log('New visit for patient:' + patientId);
            this.editText = 'Nouvelle';
        }
        this.patientService.getPatient(patientId)
            .subscribe(function (data) { return _this.patient = data; });
        if (visitId) {
            this.visitService.getVisitById(visitId)
                .subscribe(function (data) {
                _this.visitForm = _this.fb.group({
                    id: data.id,
                    date: new Date(data.date),
                    type: data.type,
                    price: data.price,
                    receiveToday: data.receiveToday,
                    paymentMethod: data.paymentMethod,
                    comment: data.comment
                });
                console.log(data);
            });
        }
    };
    Object.defineProperty(NewVisitComponent.prototype, "price", {
        get: function () {
            return this.visitForm.get('price');
        },
        enumerable: true,
        configurable: true
    });
    NewVisitComponent.prototype.paymentMethods = function () {
        var keys = Object.keys(_paymentMethod__WEBPACK_IMPORTED_MODULE_6__["PaymentMethod"]);
        return keys.slice(keys.length / 2, keys.length);
    };
    NewVisitComponent.prototype.addVisit = function () {
        var _this = this;
        var newVisit = Object.assign({}, this.visitForm.value);
        newVisit.patient = this.patient;
        console.log(newVisit);
        this.visitService.addVisit(newVisit)
            .subscribe(function (visit) {
            console.log('Visit added ' + visit);
            _this.router.navigate(['/patient/' + _this.patient.id + '/visit']);
        });
    };
    NewVisitComponent.prototype.assignPrice = function (event) {
        var consultation = event.target.value;
        if (consultation === 'BILAN') {
            this.price.setValue(40);
        }
        else if (consultation === 'SEANCE') {
            this.price.setValue(30);
        }
        else {
            this.price.setValue(35);
        }
    };
    NewVisitComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new',
            template: __webpack_require__(/*! ./new-visit.component.html */ "./src/app/ui/visit/new/new-visit.component.html"),
            providers: [
                { provide: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbDateParserFormatter"], useClass: _ngbDateCustomParserFormatter__WEBPACK_IMPORTED_MODULE_9__["NgbDateCustomParserFormatter"] }
            ],
            styles: [__webpack_require__(/*! ./new-visit.component.css */ "./src/app/ui/visit/new/new-visit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _visit_service__WEBPACK_IMPORTED_MODULE_2__["VisitService"], _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"]])
    ], NewVisitComponent);
    return NewVisitComponent;
}());



/***/ }),

/***/ "./src/app/ui/visit/new/ngbDateCustomParserFormatter.ts":
/*!**************************************************************!*\
  !*** ./src/app/ui/visit/new/ngbDateCustomParserFormatter.ts ***!
  \**************************************************************/
/*! exports provided: NgbDateCustomParserFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbDateCustomParserFormatter", function() { return NgbDateCustomParserFormatter; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var NgbDateCustomParserFormatter = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NgbDateCustomParserFormatter, _super);
    function NgbDateCustomParserFormatter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NgbDateCustomParserFormatter.prototype.toInteger = function (value) {
        return parseInt("" + value, 10);
    };
    NgbDateCustomParserFormatter.prototype.isNumber = function (value) {
        return !isNaN(this.toInteger(value));
    };
    NgbDateCustomParserFormatter.prototype.padNumber = function (value) {
        if (this.isNumber(value)) {
            return ("0" + value).slice(-2);
        }
        else {
            return '';
        }
    };
    NgbDateCustomParserFormatter.prototype.parse = function (value) {
        if (value) {
            var dateParts = value.trim().split('/');
            if (dateParts.length === 1 && this.isNumber(dateParts[0])) {
                return { day: this.toInteger(dateParts[0]), month: null, year: null };
            }
            else if (dateParts.length === 2 && this.isNumber(dateParts[0]) && this.isNumber(dateParts[1])) {
                return { day: this.toInteger(dateParts[0]), month: this.toInteger(dateParts[1]), year: null };
            }
            else if (dateParts.length === 3 && this.isNumber(dateParts[0]) && this.isNumber(dateParts[1]) && this.isNumber(dateParts[2])) {
                return { day: this.toInteger(dateParts[0]), month: this.toInteger(dateParts[1]), year: this.toInteger(dateParts[2]) };
            }
        }
        return null;
    };
    NgbDateCustomParserFormatter.prototype.format = function (date) {
        return date ?
            (this.isNumber(date.day) ? this.padNumber(date.day) : '') + "-" + (this.isNumber(date.month) ? this.padNumber(date.month) : '') + "-" + date.year :
            '';
    };
    NgbDateCustomParserFormatter = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], NgbDateCustomParserFormatter);
    return NgbDateCustomParserFormatter;
}(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"]));



/***/ }),

/***/ "./src/app/ui/visit/paymentMethod.ts":
/*!*******************************************!*\
  !*** ./src/app/ui/visit/paymentMethod.ts ***!
  \*******************************************/
/*! exports provided: PaymentMethod */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentMethod", function() { return PaymentMethod; });
var PaymentMethod;
(function (PaymentMethod) {
    PaymentMethod[PaymentMethod["CHEQUE"] = 0] = "CHEQUE";
    PaymentMethod[PaymentMethod["LIQUIDE"] = 1] = "LIQUIDE";
    PaymentMethod[PaymentMethod["VIREMENT"] = 2] = "VIREMENT";
    PaymentMethod[PaymentMethod["GIP"] = 3] = "GIP";
    PaymentMethod[PaymentMethod["DEPARTEMENT"] = 4] = "DEPARTEMENT";
    PaymentMethod[PaymentMethod["CPAM"] = 5] = "CPAM";
})(PaymentMethod || (PaymentMethod = {}));


/***/ }),

/***/ "./src/app/ui/visit/visit.component.css":
/*!**********************************************!*\
  !*** ./src/app/ui/visit/visit.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VpL3Zpc2l0L3Zpc2l0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/ui/visit/visit.component.html":
/*!***********************************************!*\
  !*** ./src/app/ui/visit/visit.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col\">\n    <h1>{{patient.forename}} {{patient.name}}</h1>\n  </div>\n  <div class=\"col\">\n    <div class=\"text-right\">\n      <a [routerLink]=\"['/patient', patient.id, 'new-visit']\">\n        <button type=\"button\" class=\"btn btn-primary btn-sm\">Nouvelle consultation</button>\n      </a>\n    </div>\n  </div>\n</div>\n<hr class=\"nb-1\">\n<div class=\"row\">\n  <div class=\"col\">\n    <table class=\"table\">\n      <thead class=\"thead-light\">\n      <tr>\n        <th scope=\"col\">#</th>\n        <th scope=\"col\">Date</th>\n        <th scope=\"col\">Type</th>\n        <th scope=\"col\">Montant de la prestation</th>\n        <th scope=\"col\">Reçu aujourd'hui</th>\n        <th scope=\"col\">Méthode de paiement</th>\n        <th scope=\"col\">Commentaire</th>\n        <th scope=\"col\"></th>\n      </tr>\n      </thead>\n      <tbody>\n      <tr *ngFor=\"let visit of visits\">\n        <td>{{visit.id}}</td>\n        <td>{{visit.date | date}}</td>\n        <td>{{visit.type}}</td>\n        <td>{{visit.price}}</td>\n        <td>{{visit.receiveToday}}</td>\n        <td>{{visit.paymentMethod}}</td>\n        <td>{{visit.comment}}</td>\n        <td>\n          <a [routerLink]=\"['/patient', patient.id, 'new-visit', visit.id]\">\n            <div style=\"cursor: pointer;\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></div>\n          </a>\n        </td>\n      </tr>\n\n      </tbody>\n    </table>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/ui/visit/visit.component.ts":
/*!*********************************************!*\
  !*** ./src/app/ui/visit/visit.component.ts ***!
  \*********************************************/
/*! exports provided: VisitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitComponent", function() { return VisitComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _visit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./visit.service */ "./src/app/ui/visit/visit.service.ts");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../patient/patient.service */ "./src/app/ui/patient/patient.service.ts");





var VisitComponent = /** @class */ (function () {
    function VisitComponent(route, visitService, patientService) {
        this.route = route;
        this.visitService = visitService;
        this.patientService = patientService;
        this.visits = [];
        this.patient = {};
    }
    VisitComponent.prototype.ngOnInit = function () {
        var _this = this;
        var patientId = Number(this.route.snapshot.paramMap.get('patientId'));
        console.log('Visit patient id is :' + patientId);
        this.getVisits(patientId);
        this.patientService.getPatient(patientId)
            .subscribe(function (data) { return _this.patient = data; });
    };
    VisitComponent.prototype.getVisits = function (patientId) {
        var _this = this;
        this.visits = [];
        this.visitService.getVisitByPatient(patientId)
            .subscribe(function (data) { return _this.visits = data; });
        return this.visits;
    };
    VisitComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-visit',
            template: __webpack_require__(/*! ./visit.component.html */ "./src/app/ui/visit/visit.component.html"),
            styles: [__webpack_require__(/*! ./visit.component.css */ "./src/app/ui/visit/visit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"], _patient_patient_service__WEBPACK_IMPORTED_MODULE_4__["PatientService"]])
    ], VisitComponent);
    return VisitComponent;
}());



/***/ }),

/***/ "./src/app/ui/visit/visit.service.ts":
/*!*******************************************!*\
  !*** ./src/app/ui/visit/visit.service.ts ***!
  \*******************************************/
/*! exports provided: VisitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitService", function() { return VisitService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var VisitService = /** @class */ (function () {
    function VisitService(http) {
        this.http = http;
        this.configUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].serviceUrl + '/visits';
    }
    VisitService.prototype.getVisitByPatient = function (patientId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set('patientId', String(patientId));
        return this.http.get(this.configUrl + '/search', { params: params });
    };
    VisitService.prototype.getVisitById = function (id) {
        return this.http.get(this.configUrl + '/' + id);
    };
    VisitService.prototype.getVisitByDate = function (date) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set('date', date.toISOString().substring(0, 10));
        return this.http.get(this.configUrl + '/search', { params: params });
    };
    VisitService.prototype.addVisit = function (visit) {
        return this.http.post(this.configUrl, visit)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
    };
    VisitService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])('Something bad happened; please try again later.');
    };
    VisitService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], VisitService);
    return VisitService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    //serviceUrl: '/assets/data/patients.json'
    serviceUrl: 'http://localhost:8080'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\projects\new-patient\angular-bootstrap\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map