package fr.valarep.visit;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/visits")
public class VisitController {

    @Autowired
    VisitService visitService;

    @GetMapping
    @ApiOperation(value = "Get all visits")
    public List<Visit> getAllVisits() {
        List<Visit> visits = new ArrayList<>();
        visits.addAll(visitService.getAllVisits());

        return visits;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a visit")
    public ResponseEntity<Visit> getById(@PathVariable long id) {
        return Optional
            .ofNullable(visitService.getVisitById(id))
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ApiOperation(value = "Add a visit")
    @ResponseStatus(HttpStatus.CREATED)
    public void addVisit(@RequestBody Visit visit) {
        visitService.addVisit(visit);
    }

    @PutMapping
    @ApiOperation(value = "Update a visit")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateVisit(@RequestBody Visit visit) {
        visitService.updateVisit(visit);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a visit")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeVisit(@PathVariable long id) {
        visitService.deleteVisit(id);
    }
}
