package fr.valarep.visit;

import java.util.List;

public interface IVisitService {

    List<Visit> getAllVisits();

    Visit getVisitById(final long id);

    boolean addVisit(final Visit visit);

    void updateVisit(final Visit visit);

    void deleteVisit(final long id);
}
