package fr.valarep.visit;

import fr.valarep.patient.Patient;
import javax.persistence.*;

import java.time.LocalDate;
import lombok.Data;

@Entity
@Data
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private LocalDate date;
    private VisitType type;
    private Patient patient;
    private double price;
    private double receiveToday;
    private PaymentMethod paymentMethod;
    private String comment;

}
