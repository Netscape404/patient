package fr.valarep.visit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitService implements IVisitService {

    @Autowired
    VisitRepository visitRepository;

    @Override
    public List<Visit> getAllVisits() {
        return visitRepository.findAll();
    }

    @Override
    public Visit getVisitById(long id) {
        return visitRepository.findById(id);
    }

    @Override
    public boolean addVisit(Visit visit) {
        List<Visit> visits = visitRepository.findByPatientAndDate(visit.getPatient(), visit.getDate());
        if (visits.size() > 0) {
            return false;
        } else {
            visitRepository.save(visit);
            return true;
        }
    }

    @Override
    public void updateVisit(Visit visit) {
        visitRepository.save(visit);
    }

    @Override
    public void deleteVisit(long id) {
        visitRepository.delete(getVisitById(id));
    }
}
