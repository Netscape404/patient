package fr.valarep.visit;

public enum PaymentMethod {

    CHEQUE, LIQUIDE, VIREMENT, GIP, DEPARTEMENT;
}
