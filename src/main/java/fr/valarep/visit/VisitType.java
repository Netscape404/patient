package fr.valarep.visit;

public enum VisitType {

    BILAN, SEANCE, ENTRETIEN;
}
