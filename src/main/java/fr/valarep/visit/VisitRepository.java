package fr.valarep.visit;

import fr.valarep.patient.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface VisitRepository extends JpaRepository<Visit, Long> {
    List<Visit> findAll();

    Visit findById(long id);

    List<Visit> findByPatientAndDate(Patient patient, LocalDate date);
}
