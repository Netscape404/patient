package fr.valarep.patient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService implements IPatientService {

    @Autowired
    PatientRepository patientRepository;

    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public Patient getPatientById(long id) {
        return patientRepository.findById(id);
    }

    @Override
    public boolean addPatient(Patient patient) {
        List<Patient> patients = patientRepository.findByForenameAndName(patient.getForename(), patient.getName());
        if (patients.size() > 0) {
            return false;
        } else {
            patientRepository.save(patient);
            return true;
        }
    }

    @Override
    public void updatePatient(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public void deletePatient(long id) {
        patientRepository.delete(getPatientById(id));
    }

    @Override
    public void deleteAllPatients() {
        patientRepository.deleteAll(getAllPatients());
    }
}
