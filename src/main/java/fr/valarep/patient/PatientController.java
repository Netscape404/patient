package fr.valarep.patient;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/patients")
public class PatientController {

    @Autowired
    PatientService patientService;

    @GetMapping
    @ApiOperation(value = "Get all patients")
    public List<Patient> getAllPatients() {
        List<Patient> patients = new ArrayList<>();
        patients.addAll(patientService.getAllPatients());

        return patients;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a patient")
    public ResponseEntity<Patient> getById(@PathVariable long id) {
        return Optional
            .ofNullable(patientService.getPatientById(id))
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ApiOperation(value = "Add a patient")
    @ResponseStatus(HttpStatus.CREATED)
    public void addPatient(@RequestBody Patient patient) {
        patientService.addPatient(patient);
    }

    @PutMapping
    @ApiOperation(value = "Update a patient")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePatient(@RequestBody Patient patient) {
        patientService.updatePatient(patient);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a patient")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removePatient(@PathVariable long id) {
        patientService.deletePatient(id);
    }

    @DeleteMapping
    @ApiOperation(value = "Delete all patients")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeAllPatients() {
        patientService.deleteAllPatients();
    }
}
