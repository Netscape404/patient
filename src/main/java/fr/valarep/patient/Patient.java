package fr.valarep.patient;

import javax.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@Data
public class Patient implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String forename;
    private String name;
}
