package fr.valarep.patient;

import java.util.List;

public interface IPatientService {

    List<Patient> getAllPatients();

    Patient getPatientById(final long id);

    boolean addPatient(final Patient patient);

    void updatePatient(final Patient patient);

    void deletePatient(final long id);

    void deleteAllPatients();
}
